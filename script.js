// Another option for calculating the average value
// const averageValue = (node, {avg, n}) => node.hasOwnProperty('value') ? {avg: (node.value + n * avg) / (n + 1), n: n + 1} : {avg, n};

const averageValue = (node, {sum, n}) => node.hasOwnProperty('value') ? {sum: node.value + sum, n: n + 1} : {sum, n};
const minNode = (node, startNode) => startNode.value < node.value ? startNode : node;
const maxNode = (node, startNode) => startNode.value > node.value ? startNode : node;
const hasChildren = node => (typeof node === 'object') && (typeof node.children !== 'undefined') && (node.children.length > 0);

const mainHandler = (graph, handler, startValue = 0) => {
    const result = handler(graph, startValue);

    if (!hasChildren(graph)) {
        return result;
    }

    return graph.children.reduce((previousValue, currentNode) => mainHandler(currentNode, handler, previousValue), result);
};

// Another option for calculating the average value
// const average = (graph) => {
//     const dataBlock = mainHandler(graph, averageValue, {avg: 0, n: 0});
//     return +dataBlock.avg.toFixed();
// };

const average = (graph) => {
    const dataBlock = mainHandler(graph, averageValue, {sum: 0, n: 0});
    return dataBlock.sum / dataBlock.n;
};
const min = (graph) => mainHandler(graph, minNode);
const max = (graph) => mainHandler(graph, maxNode);

console.log(average(graph));
console.log(min(graph));
console.log(max(graph));


